import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CreateTodoComponent} from "./components/create-todo/create-todo.component";
import {ListTodosComponent} from "./components/list-todos/list-todos.component";

const routes: Routes = [{
  path: '', component: ListTodosComponent
}, {
  path: 'add', component: CreateTodoComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
