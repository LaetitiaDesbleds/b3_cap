import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private todos: string[] = [
    'tache 1', 'tache 2', 'tache 3', 'tache 4', 'tache 5'
  ]

  constructor() {
    console.log('TodoService constructeur');
  }

  getTodos(): string[] {
    console.log('TodoService getTodos');
    return this.todos;
  }
}
