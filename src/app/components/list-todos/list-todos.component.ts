import {Component, OnInit} from '@angular/core';
import {TodoService} from "../../services/todo.service";

@Component({
  selector: 'app-list-todos',
  template: `
    <mat-list role="list" *ngIf="hasTodos; else noTodos">
      <mat-list-item role="listitem" *ngFor="let todo of todoList">{{todo}}</mat-list-item>
    </mat-list>

  <ng-template #noTodos>
    <p>There are no todos</p>
  </ng-template>`
})
export class ListTodosComponent implements OnInit{

  todoList: string[] = [];
  hasTodos: boolean = false;

  constructor(private todoService: TodoService) {
  }

  ngOnInit(): void {
    this.todoList = this.todoService.getTodos();
    this.hasTodos = this.todoList.length > 0;
  }
}
