import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  //templateUrl: './app.component.html',
  template: `
    <mat-toolbar>
      <span>{{title}}</span>
      <span class="spacer"></span>
      <div class="button-container">
        <button mat-stroked-button color="primary" [routerLink]="['']">
          List todo
          <mat-icon>list</mat-icon>
        </button>
        <button mat-stroked-button color="primary" [routerLink]="['add']">
          Add todo
          <mat-icon>add</mat-icon>
        </button>
      </div>
    </mat-toolbar>

    <router-outlet></router-outlet>

  `,
  //styleUrls: ['./app.component.scss']
  styles: [`
    .spacer {
      flex: 1 1 auto;
    }
    .button-container {
      *:not(:last-child) {
        margin-right: 1rem;
      }
    }

  `]
})
export class AppComponent {
  title = 'monSuperProjetAngular';



}
